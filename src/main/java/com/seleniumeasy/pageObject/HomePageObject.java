package com.seleniumeasy.pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/*
    Created by : Vinay.Shetty
    Date : 26-Sep-21
*/
public class HomePageObject {

    @FindBy(xpath = "//ul[@class='nav navbar-nav']//a[contains(text(),'Input Forms')]")
    WebElement navBarInputFormLink;

}
