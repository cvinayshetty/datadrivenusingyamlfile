package com.seleniumeasy.page;

import com.seleniumeasy.base.WebDriverBase;
import org.openqa.selenium.support.PageFactory;

/*
    Created by : Vinay.Shetty
    Date : 26-Sep-21
*/
public class PageObjectFactory {
    public static <T extends PageObjectClass> T create(Class<T> clazz) {
        T pageObject = null;
        try {
            pageObject = clazz.newInstance();
            PageFactory.initElements(WebDriverBase.getWebdriver(), pageObject);
        } catch (Exception e) {
            System.out.println("Error " + e);
        }
        return pageObject;
    }
}
