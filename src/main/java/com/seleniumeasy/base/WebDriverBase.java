package com.seleniumeasy.base;

import com.seleniumeasy.dataObject.config.EnvirnomentObject;
import com.seleniumeasy.helpers.Contants;
import com.seleniumeasy.helpers.FileHelper;
import com.seleniumeasy.helpers.TestDataLoader;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class WebDriverBase {
    private static WebDriver webDriver = null;
    private WebDriverWait webDriverWait = null;
    private JavascriptExecutor javascriptExecutor = null;
    private TakesScreenshot takesScreenshot = null;
    private Actions actions = null;
    FileHelper fileHelper = new FileHelper();

    public static WebDriver getWebdriver() throws Exception {
        EnvirnomentObject envirnomentObject = TestDataLoader.loadEnvironments();
        if (webDriver==null){
            if (envirnomentObject.getEnvirnomentModel().getBrowser().equalsIgnoreCase(Contants.CHROME)){
                WebDriverManager.chromedriver().setup();
                webDriver = new ChromeDriver();
                webDriver.manage().window().maximize();
            }
        }
        return webDriver;
    }
}
