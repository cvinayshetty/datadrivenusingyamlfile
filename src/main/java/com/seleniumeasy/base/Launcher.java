package com.seleniumeasy.base;

import com.seleniumeasy.dataObject.config.EnvirnomentObject;
import com.seleniumeasy.helpers.TestDataLoader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class Launcher {
    public static WebDriver driver =null;
    public static String url =null;
    public static EnvirnomentObject envirnomentObject;

    static {
        try {
            envirnomentObject = TestDataLoader.loadEnvironments();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void launchWeb() throws Exception {
        url = envirnomentObject.getEnvirnomentModel().getUrl();
        driver = WebDriverBase.getWebdriver();
        driver.get(url);
        System.out.println(url +" "+ envirnomentObject.getEnvirnomentModel().getBrowser());
        WebElement popUp = driver.findElement(By.xpath("//a[contains(text(),'No, thanks!')]"));
        try {
            if (popUp.isDisplayed()){
                popUp.click();
            }
        }catch (Exception e){
            System.out.println("No Pop Up!!");
        }
    }
}
