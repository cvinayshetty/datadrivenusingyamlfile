package com.seleniumeasy.base;

import com.seleniumeasy.anotations.BaseDataClass;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

import static com.seleniumeasy.helpers.TestDataLoader.loadTestData;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class BaseClass {
    private static final InheritableThreadLocal<Method> method = new InheritableThreadLocal<Method>();
    public static InheritableThreadLocal<? extends BaseDataClass> testData = new InheritableThreadLocal<>();

    public static InheritableThreadLocal<Method> getMethod() {
        return method;
    }

    @BeforeMethod(alwaysRun = true)
    public synchronized void baseTestMethodSetup(Method method) throws Exception {
        BaseClass.initialize(method);
    }

    public static void initialize(Method method) throws Exception {
        BaseClass.method.set(method);
        BaseClass.testData.set(loadTestData(BaseClass.getMethod().get()));
    }

    public static <T extends BaseDataClass> T getTestData() {
        return (T) testData.get();
    }
}
