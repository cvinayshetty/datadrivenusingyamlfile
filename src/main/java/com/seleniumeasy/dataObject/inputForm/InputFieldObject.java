package com.seleniumeasy.dataObject.inputForm;

import com.seleniumeasy.anotations.BaseDataClass;
import com.seleniumeasy.dataModel.inputForm.SingleInputFieldModel;
import com.seleniumeasy.dataModel.inputForm.TwoInputFieldModel;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class InputFieldObject extends BaseDataClass {
    public SingleInputFieldModel singleInputFieldModel = new SingleInputFieldModel();
    public TwoInputFieldModel twoInputFieldModel = new TwoInputFieldModel();

    public SingleInputFieldModel getSingleInputFieldModel() {
        return singleInputFieldModel;
    }

    public void setSingleInputFieldModel(SingleInputFieldModel singleInputFieldModel) {
        this.singleInputFieldModel = singleInputFieldModel;
    }

    public TwoInputFieldModel getTwoInputFieldModel() {
        return twoInputFieldModel;
    }

    public void setTwoInputFieldModel(TwoInputFieldModel twoInputFieldModel) {
        this.twoInputFieldModel = twoInputFieldModel;
    }
}
