package com.seleniumeasy.dataObject.config;

import com.seleniumeasy.dataModel.config.EnvirnomentModel;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class EnvirnomentObject {
    public EnvirnomentModel envirnomentModel = new EnvirnomentModel();

    public EnvirnomentModel getEnvirnomentModel() {
        return envirnomentModel;
    }

    public void setEnvirnomentModel(EnvirnomentModel envirnomentModel) {
        this.envirnomentModel = envirnomentModel;
    }
}
