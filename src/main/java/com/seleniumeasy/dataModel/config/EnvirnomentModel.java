package com.seleniumeasy.dataModel.config;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class EnvirnomentModel {

    private String url;
    private String browser;

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
