package com.seleniumeasy.dataModel.inputForm;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class SingleInputFieldModel {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
