package com.seleniumeasy.helpers;

import java.io.File;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class FileHelper {

    public static String getAbsolutePath(String filePath) {
        if (filePath.contains("/")) {
            filePath = filePath.replace("/", File.separator);
        }
        if (filePath.contains("\\")) {
            filePath = filePath.replace("\\", File.separator);
        }
        return new File(filePath).getAbsolutePath();

    }
}
