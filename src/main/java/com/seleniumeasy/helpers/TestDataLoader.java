package com.seleniumeasy.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.seleniumeasy.anotations.BaseDataClass;
import com.seleniumeasy.anotations.TestData;
import com.seleniumeasy.dataObject.config.EnvirnomentObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class TestDataLoader {
    public static String environmentsFile = FileHelper.getAbsolutePath("configFile/config.yaml");

    public static <T extends BaseDataClass> T loadTestData(Method method) throws IOException {
        T testData = null;
        if (method.isAnnotationPresent(TestData.class)) {
            TestData testParameters = method.getAnnotation(TestData.class);
            if ((testParameters.dataFile() != null) && !testParameters.dataFile().equals("") && (testParameters.dataClass() != null)) {
                ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
                testData = (T) mapper.readValue(new File(FileHelper.getAbsolutePath(testParameters.dataFile())), testParameters.dataClass());
            }
        }
        return testData;
    }

    public static EnvirnomentObject loadEnvironments() throws Exception {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        EnvirnomentObject environments = mapper.readValue(new File(environmentsFile), EnvirnomentObject.class);
        return environments;
    }
}
