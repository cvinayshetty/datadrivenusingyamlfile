package com.seleniumeasy.anotations;

import com.seleniumeasy.base.BaseClass;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TestData {
    String dataFile();
    Class<? extends BaseDataClass> dataClass();
}
