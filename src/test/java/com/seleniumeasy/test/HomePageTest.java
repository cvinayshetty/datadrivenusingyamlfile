package com.seleniumeasy.test;

import com.seleniumeasy.anotations.TestData;
import com.seleniumeasy.base.BaseClass;
import com.seleniumeasy.base.Launcher;
import com.seleniumeasy.dataObject.inputForm.InputFieldObject;
import org.testng.annotations.Test;

/*
    Created by : Vinay.Shetty
    Date : 25-Sep-21
*/
public class HomePageTest extends BaseClass {

    @TestData(dataFile = "testdata/inputForm.yaml", dataClass = InputFieldObject.class)
    @Test
    public void SingleInputTest() throws Exception {
        InputFieldObject inputFieldObject = getTestData();
        Launcher.launchWeb();
    }
}
